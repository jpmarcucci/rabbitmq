import pika, os, json, mysql.connector
from flask import Flask, request, render_template
app = Flask(__name__)

url = os.environ.get("RABBITMQ_URL", "127.0.0.1")
porta = os.environ.get("RABBITMQ_PORT", "5672")
usuario = os.environ.get("RABBITMQ_USER", "guest")
senha = os.environ.get("RABBITMQ_PWD", "guest")
database_host = os.environ.get("DATABASE_HOST", "127.0.0.1")
database_name = os.environ.get("DATABASE_NAME", "teste")
database_user = os.environ.get("DATABASE_USER", "root")
database_pwd = os.environ.get("DATABASE_PWD", "root")

credentials = pika.PlainCredentials(usuario,senha)
parameters = pika.ConnectionParameters(url, porta, '/', credentials)

@app.route('/')
def form():
    return render_template('index.html')

@app.route('/usuarios')
def index():
    try:
        con = mysql.connector.connect(user=database_user, password=database_pwd, host=database_host, database=database_name)
        cursor = con.cursor()
        sql = "SELECT * FROM teste;"
    except:
        return("Erro ao conectar ao banco de dados! Tente novamente em alguns instantes.")

    try:
        cursor.execute(sql)
        resultado = cursor.fetchall()

        return(str(resultado))
    except:
        return("Erro ao consultar usuarios.")

    cursor.close()
    con.close() 

    

@app.route('/', methods=['POST'])
def form_post():
    mensagem = {} 
    mensagem['nome'] = request.form['nome']
    mensagem['idade'] = request.form['idade']
    mensagem['anonascimento'] = request.form['anonascimento']
    mensagem = json.dumps(mensagem)

    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='fila', durable=True, auto_delete=False)

    channel.basic_publish(exchange='', routing_key='fila', body=mensagem)
    connection.close()
    return('Mensagem enviada a fila!')

if __name__ == '__main__':
    app.run(host='0.0.0.0')
