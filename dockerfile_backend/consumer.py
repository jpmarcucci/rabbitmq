import pika, os, json, mysql.connector

url = os.environ.get("RABBITMQ_URL", "127.0.0.1")
porta = os.environ.get("RABBITMQ_PORT", "5672")
usuario = os.environ.get("RABBITMQ_USER", "guest")
senha = os.environ.get("RABBITMQ_PWD", "guest")
database_host = os.environ.get("DATABASE_HOST", "127.0.0.1")
database_name = os.environ.get("DATABASE_NAME", "teste")
database_user = os.environ.get("DATABASE_USER", "root")
database_pwd = os.environ.get("DATABASE_PWD", "root")

def cadastra_usuarios(usuario):
    usuario = json.loads(usuario)
    con = mysql.connector.connect(user=database_user, password=database_pwd, host=database_host, database=database_name)
    cursor = con.cursor()
    sql = "INSERT INTO teste (nome, idade, anonascimento) VALUES (%s, %s, %s);"
    try:
        cursor.execute(sql, (usuario['nome'], usuario['idade'], usuario['anonascimento']))
        print("Usuario cadastrado!")
    except:
        print("Erro ao cadastrar usuario!")

    con.commit()
    cursor.close()
    con.close()

    

credentials = pika.PlainCredentials(usuario,senha)
parameters = pika.ConnectionParameters(url, porta, '/', credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='fila', durable=True, auto_delete=False)


def callback(ch, method, properties, body):
    cadastra_usuarios(body)

channel.basic_consume('fila', callback, auto_ack=True)
channel.start_consuming()
connection.close()
